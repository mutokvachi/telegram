<?php

return [
    'bot_name'     => 'okex_test_bot',
    'bot_username' => 'test_bot',
    'key'          => env('TELEGRAM_BOT_KEY'),
];
