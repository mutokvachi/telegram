<?php

namespace App\Helpers;

use App;
use Auth;
use Cookie;
use Session;
use DateTime;

class Stuff {
	public static function saveUser(){
		return Session::put('user',Auth::user());
	}

	public static function getUser(){
		return Session::get('user');
	}

	public static function uploader($file, &$name){
        $name = time().rand(0,100).$file->getClientOriginalName();
        $file->move(public_path('/main/img'), $name);
	}	

	public static function lang(){
		$lang = Cookie::get('locale');
		
		if(empty($lang))
			$lang = 'ka';

		return $lang;
	}

	public static function trans($obj, $column){
		$lang = request()->segment(1);
		if($lang == 'en' || $lang == 'ru' || $lang == 'ka'){
			$col = $column.'_'.$lang;	
		return $obj->{$col};
		}else{
			return;
		}
	}

	public static function ogShare($title, $img, $description){
		return [
			'image' => $img,
			'title' => $title,
			'description' => $description
		];
	}

	public static function imgUrl($img, $defualt = false){
		if($defualt == true)
			return '/files/'.$img;

		return '/main/img/'.$img;
	}
}