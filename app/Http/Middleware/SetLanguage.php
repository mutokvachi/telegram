<?php

namespace App\Http\Middleware;

use App;
use Cookie;
use Closure;

class SetLanguage
{
    protected $langs = ['ka', 'en', 'ru'];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($request->method() === 'GET') {
            $segment = $request->segment(1);

            $search = array_search($segment, $this->langs);
            if($search === false){
                if(Cookie::get('locale') === null)
                    $segment = 'ka';
                else
                    $segment = Cookie::get('locale');
            }

            Cookie::queue(Cookie::make('locale', $segment, 60*60*24*365));
            App::setLocale($segment);
        }
        return $next($request);
    }
}
