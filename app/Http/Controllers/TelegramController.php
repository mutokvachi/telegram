<?php

namespace App\Http\Controllers;

use App\External\Telegram\Conversation;
use Illuminate\Http\Request;

class TelegramController extends Controller {
    public function index (
        Request $request,
        Conversation $conversation
    ) {
        $conversation->handle();
    }
}
