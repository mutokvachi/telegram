<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Telegram\Bot\Api;

class SendTelegramMessage implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $chatId;
    public $message;

    public function __construct ($chatId, $message) {
        $this->chatId  = $chatId;
        $this->message = $message;
    }

    public function handle () {
        $api = new Api(config('telegram.key'));
        $api->sendMessage([
            'chat_id'      => $this->chatId,
            'text'         => $this->message['text'],
            'reply_markup' => $this->message['markup'],
            'parse_mode'   => 'markdown',
        ]);
    }
}
