<?php


namespace App\External\Telegram;


use App\Jobs\SendTelegramMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Api;

class Conversation {
    private $request;
    public $api;
    public $chatId;
    public $userMessage = false;
    public $flow;

    function __construct (Request $request) {
        $this->request = $request;
        $this->api     = new Api(config('telegram.key'));
        $this->flow    = new Flow($this);
    }

    public function handle () {
        $this->build();
        $this->process();
    }

    public function build () {
        $this->getChat();
    }

    private function getChat () { // Capture and modify webhook request
        if ($this->isCallbackMessage()) {
            $this->chatId      = $this->request->input('callback_query.message.chat.id');
            $this->userMessage = (object) [
                'type'    => 'callback',
                'message' => (object) $this->request->input('callback_query.message'),
                'data'    => $this->request->input('callback_query.data'),
            ];
        } elseif ($this->isNormalMessage()) {
            $this->chatId      = $this->request->input('message.chat.id');
            $this->userMessage = (object) [
                'type'    => 'normal',
                'message' => (object) $this->request->input('message'),
            ];
        }
    }

    public function isCallbackMessage () {
        return $this->request->input('callback_query', false);
    }

    public function isNormalMessage () {
        return $this->request->input('message.text', false);
    }

    public function process () {
        $this->flow->process();
    }

    public function sendMessage ($replyMessage) {
        SendTelegramMessage::dispatch($this->chatId, $replyMessage);
    }
}
