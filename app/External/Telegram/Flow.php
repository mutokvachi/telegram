<?php


namespace App\External\Telegram;


use App\Models\UserMessage;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class Flow {
    public $conversation;
    public $processor;

    function __construct (Conversation $conversation) {
        $this->conversation = $conversation;
        $this->processor    = new MessageProcessor($this);
    }

    public function process () {
        /*First we always process answer. Save things in database
        and it decides whether ask question again or not*/
        $this->processAnswer();
        /*Then we get question based on database results and send it to user*/
        return $this->getQuestion();
    }

    public function getChatQuestions () { // Question identificators
        return [
            'welcome',
            'enter_email',
            'confirm_email',
            'google_auth',
            'congratulations',
        ];
    }

    public function getQuestion () { // Asks question
        $key = $this->getQuestionKey();
        if (!$key) {
            // If finished run @finish method see \App\External\Telegram\MessageProcessor::finish
            return $this->processor->finish();
        }
        $func = "ask" . ucfirst(Str::camel($key)); // mapping keys to functions
        return $this->processor->{$func}();
    }

    public function getQuestionKey () { // Get which question identificator that needs to be asked.
        $chatTree = $this->getChatQuestions();
        if ($this->isStarting()) {
            $this->clearUserMessages();
        }
        if ($this->isFinished()) {
            return false;
        }
        return $this->getNextMessage($chatTree);
    }

    public function isFinished () {
        $chatTree = $this->getChatQuestions();
        return UserMessage::where('question', end($chatTree))->first();
    }

    public function getNextMessage ($chatTree) { // Get next question if last one was answered.
        $lastQuestionAnswered = $this->getLastRecord();
        if ($lastQuestionAnswered) {
            $index = array_search($lastQuestionAnswered->question, $chatTree);
            if (isset($chatTree[$index + 1])) {
                return $chatTree[$index + 1];
            }
            return end($chatTree);
        }
        return $chatTree[0];
    }

    public function isStarting () {
        return strpos($this->conversation->userMessage->message->text, '/start') === 0;
    }

    public function clearUserMessages () {
        UserMessage::where('chat_id', $this->conversation->chatId)->delete();
    }

    public function processAnswer () {
        if ($this->isStarting()) {
            return;
        }
        if ($this->isFinished()) {
            return;
        }
        $key  = $this->getLastAskedQuestion();
        $func = "process" . ucfirst(Str::camel($key));
        $this->processor->{$func}(); // Process the answer by corresponding function
    }

    public function getLastRecord () {
        return UserMessage::where('chat_id', $this->conversation->chatId)->orderByDesc('id')->first();
    }

    public function getLastAskedQuestion () { // Get which question is user answering to.
        $lastRecord = $this->getLastRecord();
        $chatTree   = $this->getChatQuestions();
        if (!$lastRecord) {
            return $chatTree[0];
        }
        $index = array_search($lastRecord->question, $chatTree);
        if (isset($chatTree[$index + 1])) {
            return $chatTree[$index + 1];
        }
        return false;
    }
}
