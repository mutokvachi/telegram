<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserMessage extends Model {
    protected $casts = [
        'answer' => 'array',
    ];
    protected $fillable = ['chat_id', 'answer', 'question'];
}
