<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
    	DB::table('users')->insert([
    		[
    			'name'    => 'admin',
	    		'email'    => 'admin@admin.ge',
	    		'password' => '$2y$10$oKYpVopCRGHeOexMIZ3mnOy2pgHQE.sJDrxsoygKPT2eIGZKDPoxu',
	    		'type'     => 'admin',
            ]
    	]);
    }
}
