### Installation

```
git clone https://gitlab.com/mutokvachi/telegram.git
composer install
cp .env.example .env
```

Then modify telegram bot credentials in `.env`
```
TELEGRAM_BOT_KEY=628711075:AAEg_0lJurAk26VwTwz13Sjw-gjA3XSngSk
```

If `QUEUE_CONNECTION` is set to `sync` in `.env` then it will synchronously 
send messages to the user. 

And if you put for example `QUEUE_CONNECTION=redis`
You just have to run `php artisan queue:work` so jobs are processed in the background.

### That's it :) 
